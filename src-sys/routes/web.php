<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//Route::get('/', function () {
//    return view('welcome');
//});

// ADD '->middleware('auth')' to routes that need authentication

Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::get('/', 'PagesController@home')->middleware('auth');
Route::get('/room', 'PagesController@reserveRoom')->middleware('auth');
Route::post('/recaptcha', 'PagesController@recaptcha');

//edit profile
Route::get('/users/{user}',  ['as' => 'users.edit', 'uses' => 'UserController@edit'])->middleware('auth');
Route::post('/users/{user}/update',  ['as' => 'users.update', 'uses' => 'UserController@update']);

//pending
//Route::get('pending/{room_reservations}',['uses' => 'PendingController@index', 'as' => 'pending.index']);
Route::get('/pending',  ['as' => 'pending.update', 'uses' => 'PendingController@update'])->middleware('auth');
Route::post('/pending/update', 'PendingController@doAction')->middleware('auth');

Route::post('/pending/save/{room_reservations}', ['uses' => 'PendingController@save', 'as' => 'pending.save'])->middleware('auth');

//search
//Route::get('/','SearchController@index');
//Route::get('/search','SearchController@search');