<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class SearchController extends Controller
{
    public function index()
 {
 return view('search.search');
 }
 public function search(Request $request)
 {
 if($request->ajax())
 {
 $output="";
 $room_reservations=DB::table('room_reservations')->where('reservee_id','LIKE','%'.$request->search."%")->get();
 
 if($room_reservations)
{
foreach ($room_reservations as $key => $room_reservation) {
$output.='<tr>'.
'<td>'.$room_reservation->reservation_id.'</td>'.
'<td>'.$room_reservation->room->room_name.'</td>'.
'<td>'.$room_reservation->users->first_name.'</td>'.
'<td>'.$room_reservation->reservee_id.'</td>'.
'<td>'.$room_reservation->date_reserved.'</td>'.
'<td>'.$room_reservation->reservation_time_start . '-'.$room_reservation->reservation_time_end.'</td>'.

'</tr>';
}
 return Response($output);
    }
    }
 }
}