<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use App\RoomReservations;
use App\User;
use Flash;
use Input;

class PendingController extends Controller
{
    public function update(){
//$from =date('Y-m-d');
//$to=date('Y-m-d');
       $room_reservations = RoomReservations::where('reservation_status', 'pending')->with('users')->get();
      
        /*return view('pending.update')->with('user')->with('room_reservations',RoomReservations::where('reservation_status', null)->get());*/
        return view('pending.update',compact('room_reservations'));
    }

    public function doAction(Request $oRequest)
    {
        $room_reservations = RoomReservations::find($oRequest->resId);
        $room_reservations->reservation_status = $oRequest->action;
        $room_reservations->save();
        Session::flash('success', 'Record has been inserted successfully'); 
        return back();
    }

    public function save(Request $request, $reservation_id){

        $room_reservations = RoomReservations::find($reservation_id);

        $room_reservations->reservation_status = $request->room_reservations;

        $room_reservations->save();

        return back();
    }
    public function current(){
        $dt = Carbon::now();
        $current = RoomReservations::where('user_id',$this->user_id)->where('reservation_status','pending')->whereBetween('date_reserved', array($from, $to))->first();
             return $current;
     }
}
