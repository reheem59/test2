<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\RoomReservation;
use App\Room;
use Auth;
use DB;

class RoomReservationsController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        $rooms = Room::paginate(5);
        return view('room.reserve')->with('rooms', $rooms);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    public function process(Request $request) 
    {
        
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        // VALIDATION RULES FOR FORM
        $this->validate($request, [
            'Date' => 'required',
            'Time-End' => 'required',
            'Time-Start' => 'required',
            'room' => 'required'
        ]);

        // ROOM RESERVATION OBJECT
        $reservation = new RoomReservation();
        $reservation->reservation_time_start = $request->input('Time-Start');
        $reservation->reservation_time_end = $request->input('Time-End');
        $reservation->date_reserved = $request->input('Date');
        $reservation->reservee_id = Auth::id();
        $reservation->room_id = DB::table('rooms')->where('room_num', $request->input('room'))->value('room_id');
        
        $request->session()->put('reservation', $reservation);

        return view('/room.room-reservation-form')->with('reservation', $reservation);

    }
    
    // SEARCH FUNCTION FOR LIVE SEARCHING
    public function search(Request $request) 
    {
        if($request->ajax()) {
            $query = $request->get('query');
            if($query != '') {
                $data = DB::table('rooms')->where('room_name', 'like', '%' .$query.'%')->paginate(5);
            }
            else {
                $data = DB::table('rooms')->orderBy('room_name', 'desc')->paginate(5);
            }
              if($data) {
                foreach($data as $room) {
                    $output .= '<tr>
                                    <td>'.$room->room_name.'<td>
                                    <td>'.$room->status.'<td>
                                    <td> --- <td>
                                </tr>';
                }
            }
            else {
                $output = '<tr>
                                <td align="center" colspan="3"> NO DATA </td>
                            </tr>';
            }
            $data = array(
                'table_data' => $output
            );
              echo json_encode($data);
        }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
}
