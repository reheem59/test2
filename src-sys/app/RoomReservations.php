<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\Model;
use App\User;

class RoomReservations extends Authenticatable
{
    use Notifiable;
    protected $primaryKey = 'reservation_id';
    protected $table = 'room_reservations';
    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'reservation_status','reservee_id','room_code','reservation_date','reservation_time_start','reservation_time_end','reservation_purpose','date_reserved','time_reserved'
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    

    public function users()
    {
        return $this->belongsTo('App\User', 'reservee_id');
    }
    public function room()
    {
        return $this->belongsTo('App\Room', 'room_code');
    }


}
