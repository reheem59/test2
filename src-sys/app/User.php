<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\Model;
use App\RoomReservations;


class User extends Authenticatable
{
    use Notifiable;
    protected $primaryKey = 'user_id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'first_name','last_name', 'email', 'password','profile_picture','access_roles','is_active',
    ];

    /**
     * The attributes that should be hidden for arrays.
     *
     * @var array
     */
    protected $hidden = [
        'password', 'remember_token',
    ];

    public function getProfilePictureAttribute($profile_picture){

        return asset($profile_picture);
    }

    public function hasRole($role)
    {
        return $this->access_roles == $role;
    }

    public function room_reservations()
    {
        return $this->hasMany('App\RoomReservations', 'reservation_id');
    }
}
