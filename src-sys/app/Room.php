<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;
use Illuminate\Database\Eloquent\Model;
use App\RoomReservations;


class Room extends Model
{
    use Notifiable;
    protected $primaryKey = 'room_id';

    /**
     * The attributes that are mass assignable.
     *
     * @var array
     */
    protected $fillable = [
        'room_name','room_type', 'floor', 'capacity','status',
    ];

    public function room_reservations()
    {
        return $this->hasMany('App\RoomReservations', 'reservation_id');
    }
}
