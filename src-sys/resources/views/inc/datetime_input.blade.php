<!--<table width='100%'>
    <tr>
        <td><p>Select Date: </p> <input type='date' name='reserve-date'></td>
        <td><p>Select Time: </p> <input type='time' name='reserve-time-start'> <p> to </p> <input type='time' name='reserve-time-end'></td>
        <td><input type='text' placeholder='search...' name='search'></td>
    </tr>
</table>-->

{!! Form::open(['url' => '/room', 'method' => 'POST']) !!}
    <div class='form-inline'>
        <div class='form-group'>
            {{Form::label('date', 'Select Date: ')}}
            {{Form::date('reserve-date', \Carbon\Carbon::now(), ['class' => 'form-control'])}}
        </div>
        <div class='form-group'>
            {{Form::label('time', 'Select Time: ')}}
            {{Form::time('reserve-date', \Carbon\Carbon::now(), ['class' => 'form-control'])}}
        <div class='form-group'>
            {{Form::label('to', ' to ')}}
            {{Form::time('reserve-date', \Carbon\Carbon::now(), ['class' => 'form-control'])}}
        </div>
        <div class='form-group'>
            {{Form::text('keyword', '', ['class' => 'form-control', 'placeholder' => 'search...'])}}
        </div>
    </div>
    
{!! Form::close() !!}