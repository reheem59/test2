@extends('layouts.app')

@section('content')
@if ($errors->any())
    <div class="alert alert-danger">
        <ul>
            @foreach ($errors->all() as $error)
                <li>{{ $error }}</li>
            @endforeach
        </ul>
    </div>
@endif


<form method="get" action="">
    {{ csrf_field() }}
    {{ method_field('patch') }}
    <div>
    <span>
    <strong >reservation_id: {{ $room_reservations->reservation_id }}</strong>
    </span>
    </div>
    <div>
    <strong >reservee_id: {{ $room_reservations->reservee_id }}</strong>
    </div>
    <div>
    <strong>room_code: {{ $room_reservations->room_code }}</strong>
    </div>
    <div>
    <strong>date_reserved: {{ $room_reservations->date_reserved }} </strong>
    </div>
    <div>
    <strong>time_reserved {{ $room_reservations->time_reserved }}</strong>
    </div>
    
    <div>
    <strong>reservation status {{ $room_reservations->reservation_status }}</strong>
    </div>
    

</form>
  

 

@endsection