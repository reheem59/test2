@extends('layouts.app')

@section('content')
<div class="form-group">
<input type="text" class="form-controller" id="search" name="search"></input>
</div>
<br/>
<div class='form-inline'>
        <div class='form-group'>
            {{Form::label('date', 'Select Date: ')}}
            {{Form::date('from', \Carbon\Carbon::now(), ['class' => 'form-control'])}}
        </div>
        <div class='form-group'>
            {{Form::label('date', 'to')}}
            {{Form::date('to', \Carbon\Carbon::now(), ['class' => 'form-control'])}}
        <div class='form-group'>
            {{Form::text('keyword', '', ['class' => 'form-control', 'placeholder' => 'search...'])}}
        </div>
    </div>

<table border="1">
    <thead align='center'>
        <th>
        Reservation ID No.
        </th>
        <th>
        Room No.
        </th>
        <th>
        Name
        </th>
        <th>
        ID No.
        </th>
        <th>
        Date
        </th>
        <th>
        Timeslot
        </th>
        <th>
        Approval Status
        </th>
       
    </thead>
<tbody>
   @foreach($room_reservations as $room_reservation)
   <tr id="reservation_{{$room_reservation->reservation_id}}" align='center'>
   <td>
   {{$room_reservation->reservation_id}}
   </td>
   <td>
   {{$room_reservation->room->room_name}}
   </td>
   <td>
   {{$room_reservation->users->first_name}}
   </td>
   <td>
   {{$room_reservation->reservee_id}}
   </td>
   <td>
   {{$room_reservation->date_reserved}}
   </td>
   <td>
   {{$room_reservation->reservation_time_start}} - {{$room_reservation->reservation_time_end}}
   </td>
   <td> @if (Session::has("success"))
   {{ Session::get("success") }}
   @endif
   <a href="#"  id="approve" onclick="doAction({{$room_reservation->reservation_id}}, 'approve')">approve</a>
   
   <a href="#" id="deny" style="color:Tomato;" onclick="doAction({{$room_reservation->reservation_id}}, 'deny')">deny</a>
   </td>
   </tr>
   
    @endforeach

 <div id='flash'></div>

 

@endsection
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script src="https://cdn.jsdelivr.net/npm/sweetalert2@8.2.6/dist/sweetalert2.all.min.js"></script>
<script type="text/javascript" src="js/pending.js"></script>


