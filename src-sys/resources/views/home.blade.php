@extends('layouts.app')
@section('pagecss')
<style>
    button {
         border-radius: 8px;
         padding: 20px;
         width: 200px;
    }
    .left-fs {
         float: left;
         width: 45%;
    }
</style>
@endsection
@section('content')
    <h3 align='center'>Home</h3>
    <hr>
    <br>
    <fieldset class='left-fs'>
        <legend>ROOM</legend>
        <table width='50%'>
        <!-- Heading in table
        <tr>
            <td class='table-head' colspan=2 align='center'><b>ROOM</b></td>
        </tr>
        -->
        <tr>
        <td>
            <form action='/room' method='get'>
                <button type='submit' onclick=>Reserve a Room</button>
            </form></td>
        <td>Process of room reservation</td>
        </tr>
        <tr>
            <td><button type='button'>View Rooms</button></td>
            <td>List of all Rooms</td>
        </tr>
        <tr>
            <td><button type='button'>My Room Reservations</button></td>
            <td>List of all my room reservations</td>
        </tr>
        <tr>
            <td>
            <form action='/pending' method='get'>
            <button type='submit'>Pending Room Reservations</button>
            </form></td>
            <td>List of pending reservations for rooms</td>
        </tr>
        <tr>
            <td><button type='button'>Approved Room Reservations</button></td>
            <td>List of approved reservations for rooms</td>
        </tr>
        </table>
    </fieldset>
    <fieldset class='right-fs'>
        <legend>VEHICLE</legend>
        <table width='50%'>
        <!-- Heading in table
        <tr>
            <td class='table-head' colspan=2 align='center'><b>ROOM</b></td>
        </tr>
        -->
        <tr>
            <td><button type='button'>Reserve a Vehicle</button></td>
            <td>Process of vehicle reservation</td>
        </tr>
        <tr>
            <td><button type='button'>View Vehicles</button></td>
            <td>List of all vehicles</td>
        </tr>
        <tr>
            <td><button type='button'>My Vehicle Reservations</button></td>
            <td>List of all my vehicle reservations</td>
        </tr>
        <tr>
            <td><button type='button'>Pending Vehicle Reservation</button></td>
            <td>List of pending reservations for vehicles</td>
        </tr>
        <tr>
            <td><button type='button'>Approved Vehicle Reservations</button></td>
            <td>List of approved reservations for vehicles</td>
        </tr>
        </table>
    </fieldset>
 </body>
 </html>
@endsection