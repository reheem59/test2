@extends('layouts.app')
@section('pagecss')
<style>
    @import url('https://fonts.googleapis.com/css?family=Roboto');
        
html, body, div, span, applet, object, iframe,
h1, h2, h3, h4, h5, h6, p, blockquote, pre,
a, abbr, acronym, address, big, cite, code,
del, dfn, em, img, ins, kbd, q, s, samp,
small, strike, strong, sub, sup, tt, var,
b, u, i, center,
dl, dt, dd, ol, ul, li,
fieldset, form, label, legend,
table, caption, tbody, tfoot, thead, tr, th, td,
article, aside, canvas, details, embed,
figure, figcaption, footer, header, hgroup,
menu, nav, output, section, summary,
time, mark, audio, video {
  margin: 0;`
  padding: 0;
  border: 0;
  font: inherit;
  font-size: 100%;
  vertical-align: baseline;
}

html {
  line-height: 1;
}

ol, ul {
  list-style: none;
}

a img {
  border: none;
}

article, aside, details, figcaption, figure, footer, header, hgroup, main, menu, nav, section, summary {
  display: block;
}

.container .login_section form h1 {
  font-weight: bold;
  font-size: 22px;
}

.container .login_section form p {
  font-weight: medium;
  font-size: 20px;
  margin-bottom: 40px;
}

.was-validated-user #user-error, .was-validated-password #pass-error {
  display: block !important;
  color: #da4644;
  font-size: 11px;
  margin-bottom: 40px;
}

.container .login_section form h1 {
  font-weight: bold;
  font-size: 22px;
  margin-bottom: 20px;
}

.container .login_section {
  background-color: #9AC4FC;
  border-radius: 0px 30px 30px 0px;
  height: 100%;
  padding-bottom: 10%;
}

@media only screen and (max-width: 575px) {
  .container .login_section form h1 {
    font-size: 28px;
    margin-top: 0;
  }
}

.container .form-row {
  background-color: #9AC4FC;
}

.container .login_section form .btn-primary,
.container .login_section form .btn-outline-primary {
  -webkit-border-radius: 20px;
  border-radius: 20px;
  margin-top: 15px;
  font-size: 12px;
  text-align: center;
}

@media only screen and (min-width: 720px) and (max-width: 991px) {
  .container .login_section form .btn-primary,
  .container .login_section form .btn-outline-primary {
    font-size: 10px;
  }
}

.container .login_section form .btn-primary {
  background-color: #2D68F5;
  border: 1px solid #2D68F5;
  padding: 7px 20px;
  text-align: center;
}

@media only screen and (min-width: 720px) and (max-width: 991px) {
  .container .login_section form .btn-primary {
    padding: 6px 30px;
    text-align: center;
  }
}

@media only screen and (min-width: 576px) and (max-width: 719px) {
  .container .login_section form .btn-primary {
    padding: 5px 15px;
  }
}

@media only screen and (max-width: 575px) {
  .container .login_section form .btn-primary {
    padding: 10px 40px;
  }
}

.container .login_section form .btn-outline-primary {
  border: 2px solid #2cabee;
  color: #2cabee;
  margin-left: 4px;
  padding: 7px 10px;
}

@media only screen and (min-width: 720px) and (max-width: 991px) {
  .container .login_section form .btn-outline-primary {
    padding: 5px 15px;
  }
}
@media only screen and (min-width: 576px) and (max-width: 719px) {
  .container .login_section form .btn-outline-primary {
    padding: 6px 20px;
  }
}

@media only screen and (max-width: 575px) {
  .container .login_section form .btn-outline-primary {
    padding: 10px 40px;
  }
}

.container .login_section form .btn-outline-primary:hover {
  background-color: #2cabee;
  color: #ffffff;
}

.container .login_section form .form-row .form-control {
  width: 280px;
  font-size: 0.85rem;
  padding: 8px 10px;
  margin-top: 10px;
}

@media only screen and (max-width: 575px) {
  .container .login_section form .form-row .form-control {
    width: 385px;
    font-size: 1.1rem;
    margin-top: 10px;
    padding: 12px 30px;
  }
}

.container .login_section form .form-check {
  margin-top: 5px;
  line-height: 1.5;
}

@media only screen and (max-width: 575px) {
  .container .login_section form .form-check {
    margin-top: 10px;
  }
}

.container .login_section form .form-check .form-check-label {
  font-size: 11px;
}

@media only screen and (max-width: 575px) {
  .container .login_section form .form-check .form-check-label, .container .login_section form .form-check a {
    font-size: 0.8rem;
  }
}

.container .login_section form .form-check .form-check-label {
  color: ;
  padding-left: 5px;
}

.error {
  display: none;
}

.was-validated-user #user-error img {
  width: 14px;
}

.was-validated-password #pass-error img {
  width: 14px;
}

body {
  background-color: #2D68F5;
  font-family: "Roboto", sans-serif;
}

.container {
  max-height: 620px;
  background-color: #ffffff;
  -moz-box-shadow: rgba(0, 0, 0, 0.4) 0 30px 100px;
  -webkit-box-shadow: rgba(0, 0, 0, 0.4) 0 30px 100px;
  box-shadow: rgba(0, 0, 0, 0.4) 0 30px 100px;
  -moz-border-radius: 5px;
  -webkit-border-radius: 5px;
  border-radius: 30px;
  margin-top: 20px;
}

@media only screen and (min-width: 992px) and (max-width: 1199px) {
  .container {
    max-height: 520px;
  }
}
@media only screen and (min-width: 720px) and (max-width: 991px) {
  .container {
    max-height: 545px;
  }
}
@media only screen and (min-width: 576px) and (max-width: 719px) {
  .container {
    max-height: 440px;
  }
}
@media only screen and (max-width: 575px) {
  .container {
    max-height: 100%;
  }
}
@media only screen and (max-width: 575px) {
  .container {
    margin-bottom: 50px;
  }
}

.container .bg_section_login {
  max-height: 620px;
  background-size: cover !important;
  position: inherit;
  border-bottom-left-radius: 20px;
  border-top-left-radius: 20px;
}

@media only screen and (min-width: 992px) and (max-width: 1199px) {
  .container .bg_section_login {
    max-height: 220px;
  }
}
@media only screen and (min-width: 720px) and (max-width: 991px) {
  .container .bg_section_login{
    max-height: 545px;
  }
}
@media only screen and (min-width: 576px) and (max-width: 719px) {
  .container .bg_section_login {
    max-height: 440px;
    display: block;
  }
}
@media only screen and (max-width: 575px) {
  .container .bg_section_login {
    display: none;
  }
}

.container .bg_section_login img {
  width: 220px;
  position: absolute;
  top: 8%;
  left: -20px;
}

.container .m_bg_section_login {
  display: none;
}

@media only screen and (max-width: 575px) {
  .container .m_bg_section_login {
    background: url("../images/login3.png") no-repeat;
    display: block;
    background-size: contain;
    padding-left: 0px;
    padding-bottom: 57%;
  }

  .container .m_bg_section_login img {
    width: 34%;
    padding-top: 6%;
    overflow: hidden;
    float: right;
  }
}

.container .bg_section_login {
  background: url("../images/login3.png") no-repeat;
}

.container .login_section {
  padding-top: 89px;
  padding-bottom: 90px;
  padding-left: 108px;
}
@media only screen and (min-width: 992px) and (max-width: 1199px) {
  .container .login_section {
    padding-top: 140px;
    padding-bottom: 100px;
  }
}

@media only screen and (min-width: 720px) and (max-width: 991px) {
  .container .login_section {
    padding-top: 130px;
    padding-left: 40px;
  }
}

@media only screen and (min-width: 576px) and (max-width: 719px) {
  .container .login_section {
    padding-top: 100px;
    padding-left: 16px;
    padding-bottom: 80px;
  }
}
@media only screen and (max-width: 575px) {
  .container .login_section {
    padding-top: 10px;
    padding-bottom: 5px;
    padding-left: 5%;
  }
}

.container .login_section form {
  padding-bottom: 10px;
}

@media only screen and (max-width: 575px) {
  .container .login_section form {
    padding-bottom: 30px;
  }
}

@media only screen and (max-width: 575px) {
  .container .login_section form .btn-primary,
  .container .login_section form .btn-outline-primary {
    font-size: 16px;
    margin-top: 30%;
  }
}

.container .login_section form .form-row {
  margin-bottom: 0;
}

.container .login_section form .form-row input:-moz-placeholder {
  color: #98a6a9;
}

.container .login_section form .form-row input::-moz-placeholder {
  color: #98a6a9;
}

.container .login_section form .form-row input:-ms-input-placeholder {
  color: #98a6a9;
}

.container .login_section form .form-row input::-webkit-input-placeholder {
  color: #98a6a9;
}

.container .login_section form .form-check a {
  margin-left: 80px;
  color: #2cabee;
  font-weight: bold;
}
@media only screen and (min-width: 720px) and (max-width: 991px) {
  .container .login_section form .form-check a {
    margin-left: 10%;
  }
}
@media only screen and (min-width: 576px) and (max-width: 719px) {
  .container .login_section form .form-check a {
    margin-left: 10%;
  }
}
@media only screen and (max-width: 575px) {
  .container .login_section form .form-check a {
    margin-left: 25%;
  }
}
.container .login_section form .form-check a:hover {
  text-decoration: none;
}

.custom-select.is-invalid, .form-control.is-invalid, .was-validated .custom-select:invalid, .was-validated .form-control:invalid {
  background-color: #fefacb;
}

.custom-select.is-valid, .form-control.is-valid, .was-validated .custom-select:valid, .was-validated .form-control:valid {
  border-color: #a2a2a2;
}

.form-row #label {
  text-align: left;
}

.form-row #input {
  text-align: left;
}

.form-check {
  vertical-align: middle;
}



</style>


@endsection
@section('content')
<!DOCTYPE html>
<html lang="en">
  <head>
    <meta charset="UTF-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1 maximum-scale=1.0"> 
    <meta name="description" content=""/>
    <meta name="author" content=""/>
    <title>Log In</title>
  </head>

  <body>
    <div class="container-fluid">
      <div class="container">
        <div class="row">

          <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 bg_section_login">
          </div>
          
          <div class="col-12 m_bg_section_login">
          </div>

          <div class="col-xl-6 col-lg-6 col-md-6 col-sm-6 col-12 login_section">
          
            <div class="form-group horizontal">
              <form method="POST" action="{{ route('login') }}">
                          @csrf
                  <form class="needs-validation" novalidate>
                  <h1>Log in</h1>
                  <p> Please login using your iACADEMY account</p>

                  <div class="form-row">
                      <div class="form-group horizontal">
                          <label for="email" class="col-lg-3 col-form-label text-md-right">{{ __('E-Mail') }}</label>
                          <div class="col-md-6">
                              <input id="email" type="email" class="form-control{{ $errors->has('email') ? ' is-invalid' : '' }}" name="email"  aria-describedby="emailHelp" placeholder="Email" value="{{ old('email') }}" required autofocus>
                              @if ($errors->has('email'))
                                  <span class="invalid-feedback" role="alert">
                                      <strong>{{ $errors->first('email') }}</strong>
                                  </span>
                              @endif
                          </div>
                      </div>
                  </div>
                  

                  <div class="form-row">
                      <div class="form-group horizontal">
                          <label for="password" class="col-md-4 col-form-label text-md-right">{{ __('Password') }}</label>
                          <div class="col-md-6">
                              <input id="password" type="password" class="form-control{{ $errors->has('password') ? ' is-invalid' : '' }}" name="password" required>
                              @if ($errors->has('password'))
                                  <span class="invalid-feedback" role="alert">
                                      <strong>{{ $errors->first('password') }}</strong>
                                  </span>
                              @endif
                          </div>
                      </div>
                  </div>

                  <div class="form-check">
                      <input type="checkbox" class="col-md-1 form-check-input" id="login_check">
                      <label class="col-md-7 form-check-label" for="login_check">Stay signed in</label>
                  </div>

                  <div class="d-flex align-items-center">
                  <input type="hidden" value="" name="recaptcha_response" id="recaptchaResponse">
                    <button type="submit" class="col-md-3 btn btn-primary">
                      {{ __('Login') }}
                    </button>
                  </div>
              </form>
            </div>
            </div>
          </div>
        </div>
      </div>
    </div> 
  </body>
</html>
@endsection


<script src="https://www.google.com/recaptcha/api.js?render=6Lcg0ooUAAAAAEHz0oqgUNTL5w-BjV5UWyxXsSD5"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.3.1/jquery.min.js"></script>
<script>
  grecaptcha.ready(function() {
      grecaptcha.execute('6Lcg0ooUAAAAAEHz0oqgUNTL5w-BjV5UWyxXsSD5', {action: 'login'}).then(function(token) {
        $.ajaxSetup({
      headers: {
            'X-CSRF-TOKEN': $('meta[name="csrf-token"]').attr('content')
        }
    });
        $.post('/recaptcha', {response:token}, function (data) {
            console.log(data);
        });
      });
  });
  </script>
