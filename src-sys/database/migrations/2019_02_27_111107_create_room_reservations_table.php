<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRoomReservationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('room_reservations');
        Schema::create('room_reservations', function (Blueprint $table) {
            $table->increments('reservation_id');
            $table->integer('reservee_id')->unsigned();
            $table->foreign('reservee_id')->references('user_id')->on('users');
            $table->integer('room_code')->unsigned();
            $table->foreign('room_code')->references('room_id')->on('rooms');
            $table->date('reservation_date');
            $table->time('reservation_time_start');
            $table->time('reservation_time_end');
            $table->string('reservation_purpose');
            $table->enum('reservation_status',['approve','deny','pending']);
            $table->date('date_reserved');
            $table->time('time_reserved');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('room_reservations');
    }
}
