<?php

use Illuminate\Support\Facades\Schema;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateRoomsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::dropIfExists('rooms');
        Schema::create('rooms', function (Blueprint $table) {
            $table->increments('room_id');
            $table->string('room_name');
            $table->enum('room_type',['normal_rooms','common_area']);
            $table->tinyInteger('floor');
            $table->integer('capacity');
            $table->integer('building_id')->unsigned();
            $table->foreign('building_id')->references('building_id')->on('buildings');
            $table->enum('status',['available','reserved','block']);
            $table->softDeletes('is_deleted');
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('rooms');
    }
}
